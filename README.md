![Exemple](exemples/exemple.jpg)
# vegutil - A CLI segmentation vegetation tool

vegutil can apply different vegetation index on image such as
- EXG (Excess of green)
- EXR (Excess of red)
- NDVI (Normalized difference vegetation index)
- NDI (Normalized difference Index)

You can either get a grayscale or segmented image using these various index.

## Installation
### with setup.py
```
$ git clone git@gitlab.com:Polensky/vegutil.git
$ cd vegutil/
$ python setup.py install
```

### Manual
```
$ git clone git@gitlab.com:Polensky/vegutil.git
$ cd vegutil/
$ pip install -r requirements.txt
```
From here you can add `vegutil` and `blober`in your `PATH` or run it directly using the `python` command.

## Usage
If you're playing with colored images, you can apply one the implemented index to produce a grayscale image.
```
$ vegutil -i exg filename.jpg
```
It will create a grayscale image named `filename_exg.jpg`.

If your images are already in grayscale, you can simple segment them with the `-s` option.
```
$ vegutil -s filename.jpg
```
It will create a black and white image named `filename_seg.jpg`.

Or you can do both by combining the `-i` and `-s` option.
```
$ vegutil -si exg filename.jpg
```
It will create a black and white image named `filename_exg_seg.jpg`.

If you want to compare your results, you can concatenate them using the `--side-by-side` option. 
```
$ vegutil --side-by-side image1.jpg image2.jpg image3.jpg
```
The images will be concantenated from left to right.
