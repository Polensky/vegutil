#!/usr/bin/env python
from distutils.core import setup

files = ['assets/*']

setup(name='vegutil',
      version='1.0',
      description='A CLI segmentation vegetation tool',
      author='Charles Sirois',
      author_email='charles.sirois@uqtr.ca',
      url='https://gitlab.com/Polensky/vegutil',
      scripts=['vegutil', 'vegutil-mi', 'blober'],
      packages=['heatmappy', 'lib'],
      package_data={'heatmappy': files},
 )
