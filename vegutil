#!/bin/python
'''
CLI to apply differents vegetation index with the ability
to apply a threshold on the image aswell
'''
import os
import warnings
import argparse
from multiprocessing import Pool
from functools import partial
import numpy as np
import piexif
from tqdm import tqdm
from skimage.morphology import disk
from skimage.exposure import histogram
from skimage import io, filters, img_as_uint
from sklearn.cluster import KMeans
warnings.filterwarnings("ignore", category=UserWarning)


def _change_scale(value, old_min, old_max, new_min=0, new_max=255):
    """transform value to fit new scale"""
    return (((value - old_min) * (new_max - new_min)) / \
            (old_max - old_min)) + new_min

def _converter_template(image, index_function):
    res = np.copy(image).astype(float)
    res = np.apply_along_axis(index_function, 2, res)
    maximum, minimum = res.max(), res.min()
    res = np.vectorize(_change_scale)(res, minimum, maximum)
    res = res.astype('uint8')
    return res

def modify_yen(image, nbins=256):
    """ to evaluate image with dead zone """
    image = image.ravel()
    # all negative values are not consider a part of the image
    image = image[image <= 0]
    hist, bin_centers = histogram(image, nbins, source_range='image')
    # On blank images (e.g. filled with 0) with int dtype, `histogram()`
    # returns ``bin_centers`` containing only one value. Speed up with it.
    if bin_centers.size == 1:
        return bin_centers[0]

    # Calculate probability mass function
    pmf = hist.astype(np.float32) / hist.sum()
    P1 = np.cumsum(pmf)  # Cumulative normalized histogram
    P1_sq = np.cumsum(pmf ** 2)
    # Get cumsum calculated from end of squared array:
    P2_sq = np.cumsum(pmf[::-1] ** 2)[::-1]
    # P2_sq indexes is shifted +1. I assume, with P1[:-1] it's help avoid
    # '-inf' in crit. ImageJ Yen implementation replaces those values by zero.
    crit = np.log(((P1_sq[:-1] * P2_sq[1:]) ** -1) *
                  (P1[:-1] * (1.0 - P1[:-1])) ** 2)
    return bin_centers[crit.argmax()]

# cch[0] => blue
# cch[1] => green
# cch[2] => red

# ndvi - maybe? maybe note
# blue = vert
# vert = rouge
# rouge = IR

def exg(image):
    """ 2g - r - b """
    func = lambda cch: (2*cch[1] - cch[2] - cch[0])
    return _converter_template(image, func)

def exr(image):
    """ 1.3r - g """
    func = lambda cch: (1.3*cch[2] - cch[1])
    return _converter_template(image, func)

def exgr(image):
    """ ExG - ExR"""
    func = lambda cch: ((2*cch[1] - cch[2] - cch[0])-(1.3*cch[2] - cch[1]))
    return _converter_template(image, func)

def ndvi(image):
    """(NIR - Red) / (NIR + Red)"""
    func = lambda cch: ((cch[0] - cch[2]) / (cch[0] + cch[2]))
    res = _converter_template(image, func)
    res = (255-res)
    return res

def ndi(image):
    """ (g - r) /  (g + r) """
    func = lambda cch: ((cch[1] - cch[2]) / (cch[1] + cch[2]))
    return _converter_template(image, func)

def kmean(image):
    """apply kmeans with 2 clusters"""
    image = image / np.linalg.norm(image)
    X = image.reshape((-1, 1))
    kmeans = KMeans(n_clusters=2)
    kmeans.fit(X)
    values = kmeans.cluster_centers_.squeeze()
    labels = kmeans.labels_
    labels.shape = (image.shape[0], image.shape[1])
    # we make sure the ground has the value 0
    if values[0] > values[1]:
        labels = 1 - labels
    return labels

def apply_threshold(image, method):
    """ segment image with the choosen method for thresholding """
    if method == 'yen':
        #binary = image >= modify_yen(image)
        binary = image >= filters.threshold_yen(image)
    elif method == 'otsu':
        binary = image >= filters.threshold_otsu(image)
    elif method == 'kmean':
        binary = kmean(image)
    elif method == 'li':
        binary = image >= filters.threshold_li(image)
    elif method == 'local':
        binary = image >= filters.threshold_local(image, 9)
        binary = ~binary

    if binary.dtype == 'bool':
        result = img_as_uint(binary).astype('uint8')
    else:
        result = binary
    if len(result.shape) >= 3:
        result = result[:, :, 0]
    result = filters.median(result, disk(3))
    return result

def apply_transformation(image_filename, indice, segment, output_format):
    """ apply the differents options given by the users """
    image = io.imread(image_filename)
    if indice and not segment:
        postfix = f'{indice}'
        result = globals()[indice](image)
    if segment and not indice:
        postfix = f'{segment}'
        result = apply_threshold(image, segment)
    if segment and indice:
        postfix = f'{indice}_{segment}'
        result = globals()[indice](image)
        result = apply_threshold(result, segment)

    filename = os.path.basename(image_filename)
    filename, extension = os.path.splitext(filename)
    output_extension = output_format if output_format else 'png'
    result_filename = f'{filename}_{postfix}.{output_extension}'
    io.imsave(result_filename, result)

    # Transfer exif data if its a JPEG image
    if extension in ('.jpg', '.JPG'):
        piexif.transplant(image, result_filename)


if __name__ == '__main__':
    PARSER = argparse.ArgumentParser()
    PARSER.add_argument(
        '-i', '--indice',
        metavar='ind',
        type=str,
        choices=['exg', 'exr', 'exgr', 'ndvi', 'ndi'],
        help=('Apply a vegetation index to an image '
              'and return a grayscale image '
              '(exg, exr, ndvi, ndi)')
    )
    PARSER.add_argument(
        '-s', '--segment',
        metavar='seg',
        type=str,
        choices=['yen', 'otsu', 'li', 'local', 'kmean'],
        help=('Segment a grayscale image and return '
              'a binary image (white foreground, black background)')
    )
    PARSER.add_argument(
        'images',
        metavar='img',
        type=str,
        nargs='+',
        help='image(s) filepath',
    )
    PARSER.add_argument(
        '--output-format',
        type=str,
        choices=['png', 'tif', 'jpg'],
        help=('The image format for the ouput(png, tif, jpg)')
    )

    ARGS = PARSER.parse_args()

    if not (ARGS.segment or ARGS.indice):
        PARSER.error('No action requested, use -i or/and -s')

    part_apply_transformation = partial(
        apply_transformation,
        indice=ARGS.indice,
        segment=ARGS.segment,
        output_format=ARGS.output_format
    )
    # Pool(None) -> use all the threads
    with Pool(None) as p:
        list(tqdm(p.imap(part_apply_transformation, ARGS.images), total=len(ARGS.images)))
